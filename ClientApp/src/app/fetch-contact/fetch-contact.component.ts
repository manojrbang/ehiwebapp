import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/models/contact';
import { Store, select } from "@ngrx/store";
import { AppState } from '../state/app.state';
import { Observable } from 'rxjs';
import { FetchContact, DeleteContact } from 'src/app/state/actions/contact.actions';
import { getContacts } from 'src/app/state/reducers/contact.reducer';

@Component({
  selector: 'app-fetch-contact',
    templateUrl: './fetch-contact.component.html',
    styleUrls: ['./fetch-contact.component.css']
})
export class FetchContactComponent implements OnInit {

  loading$: Observable<Boolean>;
  error$: Observable<Error>

    public contactList: Observable<Contact[]>;

    temp: Contact[];

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(FetchContact());
    this.contactList = this.store.pipe(select(getContacts));
    this.loading$ = this.store.select(store => store.contact.loading);
  }

    delete(contactId) {
    const ans = confirm('Do you want to delete contact with Id: ' + contactId);
    if (ans) {
        this.store.dispatch(DeleteContact({ id: contactId }));
    }
  }
}
