import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FetchContactComponent } from './fetch-contact.component';

describe('FetchContactComponent', () => {
    let component: FetchContactComponent;
    let fixture: ComponentFixture<FetchContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [FetchContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(FetchContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
