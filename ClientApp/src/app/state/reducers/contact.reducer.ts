

import { createSelector, createReducer, on } from "@ngrx/store";
import { Action } from "@ngrx/store/src/models";
import { AppState } from "../app.state";
import { Contact } from "src/models/contact";
import * as ContactActions from "src/app/state/actions/contact.actions";

export interface ContactState {
    list: Contact[],
    loading: boolean,
    error: Error
}

export const initialState = {
    list: [],
    loading: false,
    error: undefined
};

export const contactReducer = createReducer(
    initialState,
    on(ContactActions.FetchContact, state => (console.log('fetch reducer called'), {
        ...state,
        loading: true,

    })),
    on(ContactActions.LoadContactSuccess, (state, { contacts }) => (
        (console.log('LoadContactSuccess reducer called'),
            {
                ...state,
                list: contacts,
                loading: false
            })),
    ),

    on(ContactActions.LoadContactFailure, (state, { error }) => (
        (console.log('LoadContactSuccess reducer called'), {
            ...state,
            error: error,
            loading: false
        })),
    ),

    on(ContactActions.AddContact, state =>
        (console.log('AddContact reducer called'), {
            ...state,
            loading: true,
        })),

    on(ContactActions.AddContactSuccess, (state, { contact }) =>
        (console.log('AddContactSuccess reducer called'), {
            ...state,
            list: [...state.list, contact],
            loading: false,
        })),

    on(ContactActions.AddContactFailure, (state, { error }) => (
        (console.log('AddContactFailure reducer called'), {
            ...state,
            error: error,
            loading: false
        })),
    ),

    on(ContactActions.EditContact, state =>
        (console.log('EditContact reducer called'), {
            ...state,
            loading: true,
        })),

    on(ContactActions.EditContactSuccess, (state, { contact }) => ({
        ...state,
        list: [...state.list, contact],
        loading: false,
    })),

    on(ContactActions.EditContactFailure, (state, { error }) => (
        (console.log('EditContactFailure reducer called'), {
            ...state,
            error: error,
            loading: false
        })),
    ),

    on(ContactActions.DeleteContact, state =>
        (console.log('DeleteContact reducer called'), {
            ...state,
            loading: true,

        })),

    on(ContactActions.DeleteContactSuccess, (state, { id }) =>
        (console.log('DeleteContactSuccess reducer called'), {
            ...state,
            list: state.list.filter(item => item.contactId !== id),
            loading: false,
        })),
);

export function reducer(state: ContactState | undefined, action: Action) {
    return contactReducer(state, action);
}

const getContactFeatureState = (state: AppState) => state.contact;

export const getContacts = createSelector(
    getContactFeatureState,
    (state: ContactState) => state.list
);
