import { Contact } from "src/models/contact";
import { createAction, props } from '@ngrx/store';

export enum ContactActionTypes {
    FETCH_CONTACT = "[CONTACT] Fetch Contact",
    FETCH_CONTACT_SUCCESS = "[CONTACT] Fetch Contact Success",
    FETCH_CONTACT_FAILURE = "[CONTACT] Fetch Contact Failed",
    ADD_CONTACT = '[CONTACT] Add',
    ADD_CONTACT_SUCCESS = "[CONTACT] Add Contact Success",
    ADD_CONTACT_FAILURE = "[CONTACT] Add Contact Failed",
    EDIT_CONTACT = '[CONTACT] EDIT',
    EDIT_CONTACT_SUCCESS = "[CONTACT] EDIT Contact Success",
    EDIT_CONTACT_FAILURE = "[CONTACT] EDIT Contact Failed",
    DELETE_CONTACT = "[CONTACT] Delete Contact",
    DELTE_CONTACT_SUCCESS = "[CONTACT] Delete Contact Success",
    DELETE_CONTACT_FAILURE = "[CONTACT] Delete Contact Failed",
}

export const FetchContact = createAction(
    ContactActionTypes.FETCH_CONTACT
)

export const LoadContactSuccess = createAction(
    ContactActionTypes.FETCH_CONTACT_SUCCESS,
    props<{ contacts: Contact[] }>()
)

export const LoadContactFailure = createAction(
    ContactActionTypes.FETCH_CONTACT_FAILURE,
    props<{ error: any }>()
)

export const AddContact = createAction(
    ContactActionTypes.ADD_CONTACT,
    props<{ contact: Contact }>()
)

export const AddContactSuccess = createAction(
    ContactActionTypes.ADD_CONTACT_SUCCESS,
    props<{ contact: Contact }>()
)

export const AddContactFailure = createAction(
    ContactActionTypes.ADD_CONTACT_FAILURE,
    props<{ error: any }>()
)

export const EditContact = createAction(
    ContactActionTypes.EDIT_CONTACT,
    props<{ contact: Contact }>()
)

export const EditContactSuccess = createAction(
    ContactActionTypes.EDIT_CONTACT_SUCCESS,
    props<{ contact: Contact }>()
)

export const EditContactFailure = createAction(
    ContactActionTypes.EDIT_CONTACT_FAILURE,
    props<{ error: any }>()
)

export const DeleteContact = createAction(
    ContactActionTypes.DELETE_CONTACT,
    props<{ id: number }>()
)
export const DeleteContactSuccess = createAction(
    ContactActionTypes.DELTE_CONTACT_SUCCESS,
    props<{ id: number }>()
)
