import { Injectable } from "@angular/core";
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { ContactService } from "src/app/services/contact.service";
import { mergeMap, map, catchError, tap, switchMap } from "rxjs/operators";
import {
    LoadContactSuccess, FetchContact, AddContact, AddContactSuccess,
    DeleteContact, DeleteContactSuccess, EditContact, EditContactSuccess,
    LoadContactFailure, AddContactFailure, EditContactFailure
} from "src/app/state/actions/contact.actions";
import { of } from 'rxjs';
import { Router } from "@angular/router";

@Injectable()
export class ContactEffect {
    constructor(
        private actions$: Actions,
        private _contactService: ContactService,
        private _router: Router,
    ) { }

    loadContact$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FetchContact),
            switchMap(() =>
                this._contactService.getContacts().pipe(
                    map((contacts) => LoadContactSuccess({ contacts })),
                    catchError(error => of(LoadContactFailure({ error })))
                )
            )
        ),
    )

    addContact$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AddContact),
            mergeMap(({ contact }) =>
                this._contactService.saveContact(contact).pipe(
                    map(() => AddContactSuccess({ contact })),
                    tap(() => this._router.navigate(['/fetch-contact'])),
                    catchError(error => of(AddContactFailure({ error })))
                ),
            )
        )
    )

    editContact$ = createEffect(() =>
        this.actions$.pipe(
            ofType(EditContact),
            mergeMap(({ contact }) =>
                this._contactService.updateContact(contact).pipe(
                    map(() => EditContactSuccess({ contact })),
                    tap(() => this._router.navigate(['/fetch-contact'])),
                    catchError(error => of(EditContactFailure({ error })))
                ),
            )
        )
    )

    deleteContact$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DeleteContact),
            mergeMap(({ id }) =>
                this._contactService.deleteContact(id).pipe(
                    map(() => DeleteContactSuccess({ id })),
                )
            ),
        )
    )
}
