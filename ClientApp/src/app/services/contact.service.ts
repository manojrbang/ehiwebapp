import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Contact } from 'src/models/contact';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ContactService {

    myAppUrl = '';

    constructor(private _http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
        this.myAppUrl = baseUrl;
    }


    getContacts() {
        console.log('service called');
        return this._http.get<Contact[]>(this.myAppUrl + 'api/Contact/Index').pipe(map(
            response => {
                return response;
            }));
    }

    getContactById(id: number) {
        return this._http.get(this.myAppUrl + 'api/Contact/Details/' + id)
            .pipe(map(
                response => {
                    return response;
                }));
    }

    saveContact(contact: Contact) {
        console.log('service called');
        return this._http.post(this.myAppUrl + 'api/Contact/Create', contact)
            .pipe(map(
                response => {
                    return response;
                }));
    }

    updateContact(contact: Contact) {
        return this._http.put(this.myAppUrl + 'api/Contact/Edit', contact)
            .pipe(map(
                response => {
                    return response;
                }));
    }

    deleteContact(id: number): Observable<any> {
        return this._http.delete(this.myAppUrl + 'api/Contact/Delete/' + id)
            .pipe(map(
                response => {
                    return response;
                }));
    }
}
