import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from '../services/contact.service';
import { Contact } from 'src/models/contact';
import { AppState } from '../state/app.state';
import { Store } from '@ngrx/store';
import { AddContact, EditContact } from '../state/actions/contact.actions';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent implements OnInit {

  contactForm: FormGroup;
  title = 'Create';
  contactId: number;
  errorMessage: any;

  constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute,
    private _contactService: ContactService, private _router: Router,
    private store: Store<AppState>) {
    if (this._avRoute.snapshot.params['id']) {
      this.contactId = this._avRoute.snapshot.params['id'];
    }

    this.contactForm = this._fb.group({
      contactId: 0,
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
        email: ['', [Validators.required]],
      phoneNumber:[''],
        status: ['', [Validators.required]]

    })
  }

  ngOnInit() {
    //this._contactService.getCityList().subscribe(
    //  (data: City[]) => this.cityList = data
    //);


      if (this.contactId > 0) {
          this.title = 'Edit';
          this._contactService.getContactById(this.contactId)
              .subscribe((response: Contact) => {
                  this.contactForm.setValue(response);
              }, error => console.error(error));
      }
  }


  save() {

    if (!this.contactForm.valid) {
      return;
    }

    if (this.title === 'Create') {
      this.store.dispatch(AddContact({ contact: this.contactForm.value }));
    } else if (this.title === 'Edit') {
      
      this.store.dispatch(EditContact({ contact: this.contactForm.value }));
    }
  }

  cancel() {
    this._router.navigate(['/fetch-contact']);
  }

    get firstName() { return this.contactForm.get('firstName'); }
    get lastName() { return this.contactForm.get('lastName'); }
    get email() { return this.contactForm.get('email'); }
    get phoneNumber() { return this.contactForm.get('phoneNumber'); }
  get status() { return this.contactForm.get('status'); }
 
}
