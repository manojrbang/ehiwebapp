export class Contact {
    contactId: number;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    status: string;
}
