﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EFNgApp.Interfaces;
using EFNgApp.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EFNgApp.Controllers
{
    [Route("api/[controller]")]
    public class ContactController : Controller
    {
        private readonly IContact objContact;

        public ContactController(IContact _objContact)
        {
            objContact = _objContact;
        }

        [HttpGet]
        [Route("Index")]
        public IEnumerable<TblContact> Index()
        {
            return objContact.GetAllContacts();
        }

        [HttpPost]
        [Route("Create")]
        public int Create([FromBody] TblContact Contact)
        {
            return objContact.AddContact(Contact);
        }

        [HttpGet]
        [Route("Details/{id}")]
        public TblContact Details(int id)
        {
            return objContact.GetContactData(id);
        }

        [HttpPut]
        [Route("Edit")]
        public int Edit([FromBody]TblContact Contact)
        {
            return objContact.UpdateContact(Contact);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public int Delete(int id)
        {
            return objContact.DeleteContact(id);
        }

 
    }
}
