﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFNgApp.Models;

namespace EFNgApp.Interfaces
{
    public interface IContact
    {
        IEnumerable<TblContact> GetAllContacts();
        int AddContact(TblContact contact);
        int UpdateContact(TblContact contact);
        TblContact GetContactData(int id);
        int DeleteContact(int id);
    }
}
