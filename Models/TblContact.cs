﻿using System;
using System.Collections.Generic;

namespace EFNgApp.Models
{
    public partial class TblContact
    {
        public int ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Status { get; set; }
    }
}
