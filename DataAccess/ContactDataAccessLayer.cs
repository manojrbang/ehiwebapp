﻿using EFNgApp.Interfaces;
using EFNgApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace EFNgApp.DataAccess
{
    public class ContactDataAccessLayer : IContact
    {
        private EHIContactStoreDBContext db;

        public ContactDataAccessLayer(EHIContactStoreDBContext _db)
        {
            db = _db;
        }

        public IEnumerable<TblContact> GetAllContacts()
        {
            try
            {
                return db.TblContact.ToList().OrderBy(x => x.ContactId);
            }
            catch
            {
                throw;
            }
        }

        //To Add new contact record 
        public int AddContact(TblContact contact)
        {
            try
            {
                db.TblContact.Add(contact);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //To Update the records of a particluar contact
        public int UpdateContact(TblContact contact)
        {
            try
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();

                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Get the details of a particular contact
        public TblContact GetContactData(int id)
        {
            try
            {
                TblContact contact = db.TblContact.Find(id);
                return contact;
            }
            catch
            {
                throw;
            }
        }

        //To Delete the record on a particular contact
        public int DeleteContact(int id)
        {
            try
            {
                TblContact con = db.TblContact.Find(id);
                db.TblContact.Remove(con);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

       
    }
}
